import React from "react";
import "../App.css";
import "../css/register.css";
const Users = (props) => {
 
  const {
    email,
    setEmail,
    password,
    setPassword,
    handleLogin,
    handleSignup,
    hasAccount,
    setHasAccount,
    emailError,
    passwordError,
} = props;


  return (
<section className="login">

<div class="container">
    <form className="m-5">
        <ul>
            <li>
                <label for="name"><span>Correo Electronico <span class="required-star">*</span></span></label>
                <input type="text" autoFocus required value={email} onChange={(e) => setEmail (e.target.value)} placeholder="Correo Electronico"/>
                <p className="errorMsg">{emailError}</p>
            </li>
            <li>
                <label for="mail"><span>Contraseña <span class="required-star">*</span></span></label>
                <input type="password" required value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Contraseña"/>
                <p className="errorMsg">{passwordError}</p>

            </li>
            <li>
              {
                <>
                        <button className="buttonR" onClick={handleSignup}>Registrarte</button>
                </>
              }
            </li>
        </ul>
    </form>
</div>
</section>
  );
  }
export default Users;
