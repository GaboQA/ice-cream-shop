import React, { useState, useEffect } from "react";
//import "bootstrap/dist/css/bootstrap.min.css";
import { db } from "../firebase";

const LinkForm = (props) => {
  const initialStateValues = {
    url: "",
    name: "",
    description: "",
  };
  const [values, setValues] = useState(initialStateValues);
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setValues({ ...values, [name]: value });
    //console.log(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.addOrEditLink(values);
    setValues({ ...initialStateValues });
  };
  const getLinkById = async (id) => {
    const doc = await db.collection("links").doc(id).get();
    setValues({ ...doc.data() });
  };
  useEffect(() => {
    if (props.currentId === "") {
      setValues({ ...initialStateValues });
    } else {
      getLinkById(props.currentId);
      console.log(props.currentId);
    }
  }, [props.currentId]);
  return (
    <form className="card card-body" onSubmit={handleSubmit}>
      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">store_mall_directory</i>
        </div>
        <input
          type="text"
          className="form-control"
          placeholder="Nombre"
          name="nombre"
          onChange={handleInputChange}
          value={values.nombre}
        ></input>
      </div>

      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">flutter_dash</i>
        </div>
        <input
          type="number"
          id="quantity"
          name="quantity"
          min="0"
          max="100"
          className="form-control"
          placeholder="Cantidad"
          name="cantidad"
          onChange={handleInputChange}
          value={values.cantidad}
        ></input>
      </div>

      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">request_page</i>
        </div>
        <input
          type="number"
          id="quantity"
          name="quantity"
          min="0"
          max="100"
          className="form-control"
          placeholder="Colones"
          name="precio"
          onChange={handleInputChange}
          value={values.precio}
        ></input>
      </div>

      <div className="form-group input-group">
        <div className="input-group-text bg-light">
          <i className="material-icons">add_photo_alternate</i>
        </div>
        <input
          type="image"
          className="form-control"
          placeholder="Imagen"
          name="imagen"
          onChange={handleInputChange}
          value={values.imagen}
          disabled
        ></input>
      </div>

      <div className="form-group">
        <textarea
          name="description"
          rows="3"
          className="form-control"
          placeholder="Detalles del producto (opcional)"
          onChange={handleInputChange}
          value={values.description}
        ></textarea>
      </div>
      <button className="btn btn-primary btn-block">
        {props.currentId === "" ? "Guardar" : "Actualizar"}
      </button>
    </form>
  );
};
export default LinkForm;
