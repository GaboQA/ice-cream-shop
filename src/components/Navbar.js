import "../App.css";
import * as ReactBootStrap from "react-bootstrap";
import Profits from "./Profits";
import Links from "./Links";  
import Start from "./Start";  
import React, {useState, useEffect }  from "react"; 
import login from '../firebase/login'
import Users from "./Users";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink 
} from "react-router-dom";

function Navbar({handleLogout}) {
  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [hasAccount, setHasAccount] = useState(false);
  
  
  const clearErrors = () => {
    setEmailError('');
    setPasswordError('');
  }

  const handleSignup = () => {
    clearErrors();
    login
    .auth()
    .createUserWithEmailAndPassword(email, password) 
    .catch((err) => {
      switch(err.code){
        case "auth/email-already-in-use":
          case "auth/invalid-email":
              setEmailError(err.message);
              break; 
            case "auth/weak-password":
              setPasswordError(err.message);
              break;
        }

    });
  };

  return (
    <Router>
    <section className="Navbar">
    <div className="Appnav">
      <ReactBootStrap.Navbar bg="light" expand="lg">
        <ReactBootStrap.Navbar.Brand>
          Heladeria RM
        </ReactBootStrap.Navbar.Brand>
        <ReactBootStrap.Navbar.Toggle aria-controls="basic-navbar-nav" />
        <ReactBootStrap.Navbar.Collapse id="basic-navbar-nav">
          <ReactBootStrap.Nav className="mr-auto">
            <Link className="navlinks" to="/">
              Inicio
            </Link>
            <Link className="navlinks" to="/Links">
              Mantenimiento
            </Link>
            <Link  className="navlinks" to="/Profits">
              Reportes               
            </Link>    
            <Link className="navlinks" to="/Users">
              Usuarios
            </Link>   
            <Link className="navlinks" onClick={handleLogout} to="/">
              Cerrar sesión 
            </Link> 
          </ReactBootStrap.Nav>
          <ReactBootStrap.Form inline></ReactBootStrap.Form>
        </ReactBootStrap.Navbar.Collapse>
      </ReactBootStrap.Navbar>
    </div>
    <Switch>
    <Route path="/" exact>
     <Start />
    </Route>
    <Route path="/Links" exact>
     <Links />
    </Route>
    <Route path="/Profits" exact>
     <Profits />
    </Route>
    <Route path="/Users" exact>
    <div className="App">
      <div className="container p-4">
      
      
      {user ? (
      <Navbar handleLogout={handleLogout} />
    ) : (
      <Users email={email} setEmail={setEmail} password={password} setPassword={setPassword} handleSignup={handleSignup} hasAccount={hasAccount} setHasAccount={setHasAccount} emailError={emailError}  passwordError={passwordError} />      )}
      <div className="container p-4">
      <ToastContainer />
      </div>
    </div>
    </div></Route>
    </Switch>
    </section>
    </Router>
  );
}

export default Navbar;

/*
Buscador
<div className="container p-4">
<ReactBootStrap.FormControl type="text" placeholder="Producto" className="mr-sm-2" />
<div className="container p-4">
<ReactBootStrap.Button variant="outline-success">
Buscar
</ReactBootStrap.Button>
</div>
</div>



import React from "react";
import "../App.css";
import * as ReactBootStrap from "react-bootstrap";
import Profits from "./Profits";
import Links from "./Links";  
import Start from "./Start";  
import Users from "./Users";  

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  NavLink 
} from "react-router-dom";

function Navbar({handleLogout}) {
  return (
    <Router>
    <section className="Navbar">
    <div className="App">
      <ReactBootStrap.Navbar bg="light" expand="lg">
        <ReactBootStrap.Navbar.Brand>
          Heladeria RM
        </ReactBootStrap.Navbar.Brand>
        <ReactBootStrap.Navbar.Toggle aria-controls="basic-navbar-nav" />
        <ReactBootStrap.Navbar.Collapse id="basic-navbar-nav">
          <ReactBootStrap.Nav className="mr-auto">
            <ReactBootStrap.Nav.Link href="/">
              Inicio
            </ReactBootStrap.Nav.Link>
            <ReactBootStrap.Nav.Link href="/Links">
              Mantenimiento
            </ReactBootStrap.Nav.Link>
            <ReactBootStrap.Nav.Link href="/Profits">
              Reportes               
            </ReactBootStrap.Nav.Link>    
            <ReactBootStrap.Nav.Link href="/Users">
              Usuarios
            </ReactBootStrap.Nav.Link>   
            <ReactBootStrap.Nav.Link onClick={handleLogout} href="/">
              Cerrar sesión 
            </ReactBootStrap.Nav.Link> 
          </ReactBootStrap.Nav>
          <ReactBootStrap.Form inline></ReactBootStrap.Form>
        </ReactBootStrap.Navbar.Collapse>
      </ReactBootStrap.Navbar>
    </div>
    <Switch>
    <Route path="/" exact>
     <Start />
    </Route>
    <Route path="/Links" exact>
     <Links />
    </Route>
    <Route path="/Profits" exact>
     <Profits />
    </Route>
    <Route path="/Users" exact>
     <Users />
    </Route>
    </Switch>
    </section>
    </Router>
  );
}

export default Navbar;

/*
Buscador
<div className="container p-4">
<ReactBootStrap.FormControl type="text" placeholder="Producto" className="mr-sm-2" />
<div className="container p-4">
<ReactBootStrap.Button variant="outline-success">
Buscar
</ReactBootStrap.Button>
</div>
</div>*/

