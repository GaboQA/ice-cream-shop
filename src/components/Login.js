import React from 'react';
const Login = (props) => {

    const {
        email,
        setEmail,
        password,
        setPassword,
        handleLogin,
        handleSignup,
        hasAccount,
        setHasAccount,
        emailError,
        passwordError,
    } = props;

    return(
        <section className="login">
             <div className="loginContainer">
                 <div className="dfimg">
                 <center className="a p-4">
                    <img className="imglogin" src="https://image.freepik.com/vector-gratis/tienda-helados-vector-icono-ilustracion-concepto-icono-edificio-punto-referencia-blanco-aislado_138676-434.jpg" />
                </center>
                </div>
             <h1 class="form__heading p-2">Bienvenido</h1>
                <input type="text" autoFocus required value={email} onChange={(e) => setEmail (e.target.value)} placeholder="Correo"/>
                <p className="errorMsg">{emailError}</p>
                <input type="password" required value={password} onChange={(e) => setPassword(e.target.value)} placeholder="Contraseña"/>
                <p className="errorMsg">{passwordError}</p>
                
                    {
                        <>
                        <button className="btnLogin" onClick={handleLogin} >Iniciar sesion</button>
                        </>
                    }
                
             </div>
        </section>
    );
};

export default Login;

/**
 * 
 */