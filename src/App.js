import './App.css';
import React, {useState, useEffect }  from "react"; 
import login from './firebase/login'
import Login from "./components/Login";
import Users from "./components/Users";
import Navbar from "./components/Navbar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const App = () => {

  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [hasAccount, setHasAccount] = useState(false);

  const clearInputs = () => {
    setEmail('');
    setPassword('');
  }
  const clearErrors = () => {
    setEmailError('');
    setPasswordError('');
  }

  const handleLogin = () => {
    clearErrors();
    login
    .auth()
    .signInWithEmailAndPassword(email, password) 
    .catch((err) => {
      switch(err.code){
        case "auth/invalid-email":
          case "auth/user-disabled":
            case "auth/user-not-found":
              setEmailError(err.message);
              break; 
            case "auth/wrong-password":
              setPasswordError(err.message);
              break;

      }

    });
  };


  const handleSignup = () => {
    clearErrors();
    login
    .auth()
    .createUserWithEmailAndPassword(email, password) 
    .catch((err) => {
      switch(err.code){
        case "auth/email-already-in-use":
          case "auth/invalid-email":
              setEmailError(err.message);
              break; 
            case "auth/weak-password":
              setPasswordError(err.message);
              break;
        }

    });
  };

  const handleLogout = () =>  {
    login.auth().signOut();

  };
  const authListener = () => {
    login.auth().onAuthStateChanged((user) => {
      if(user) {
        clearInputs();
        setUser(user);
      }else {
        setUser('');
      }
    });

  };

  useEffect(() => {
    authListener();
  }, []);

  return (
    
    <div className="App">
      <div className="container p-4">
      
      
      {user ? (
      <Navbar handleLogout={handleLogout} />
    ) : (
      <Login email={email} setEmail={setEmail} password={password} setPassword={setPassword} handleLogin={handleLogin} handleSignup={handleSignup} hasAccount={hasAccount} setHasAccount={setHasAccount} emailError={emailError}  passwordError={passwordError} />      )}
      <div className="container p-4">
      <ToastContainer />
      </div>
    </div>
    </div>
  );
};

export default App;







/*import React from "react";
import "./App.css";
//import "bootstrap/dist/css/bootstrap.min.css";
import Links from "./components/Links";
import Navbar from "./components/Navbar";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//className="row"
function App() {
  return (
    <div className="container p-4">
      <Navbar />
      <ToastContainer />
    </div>
  );
}

export default App;*/
/*import './App.css';
import React, {useState, useEffect }  from "react"; 
import login from './firebase/login'
import Login from "./components/Login";
import Navbar from "./components/Navbar";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const App = () => {

  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [hasAccount, setHasAccount] = useState(false);

  const clearInputs = () => {
    setEmail('');
    setPassword('');
  }
  const clearErrors = () => {
    setEmailError('');
    setPasswordError('');
  }

  const handleLogin = () => {
    clearErrors();
    login
    .auth()
    .signInWithEmailAndPassword(email, password) 
    .catch((err) => {
      switch(err.code){
        case "auth/invalid-email":
          case "auth/user-disabled":
            case "auth/user-not-found":
              setEmailError(err.message);
              break; 
            case "auth/wrong-password":
              setPasswordError(err.message);
              break;

      }

    });
  };


  const handleSignup = () => {
    clearErrors();
    login
    .auth()
    .createUserWithEmailAndPassword(email, password) 
    .catch((err) => {
      switch(err.code){
        case "auth/email-already-in-use":
          case "auth/invalid-email":
              setEmailError(err.message);
              break; 
            case "auth/weak-password":
              setPasswordError(err.message);
              break;
        }

    });
  };

  const handleLogout = () =>  {
    login.auth().signOut();

  };
  const authListener = () => {
    login.auth().onAuthStateChanged((user) => {
      if(user) {
        clearInputs();
        setUser(user);
      }else {
        setUser('');
      }
    });

  };

  useEffect(() => {
    authListener();
  }, []);

  return (
    
    <div className="App">
      <div className="container p-4">
      
      
      {user ? (
      <Navbar handleLogout={handleLogout} />
    ) : (
      <Login email={email} setEmail={setEmail} password={password} setPassword= {setPassword} handleLogin={handleLogin} handleSignup={handleSignup} hasAccount={hasAccount} setHasAccount={setHasAccount} emailError={emailError}  passwordError={passwordError} /> 
      )}
      <div className="container p-4">
      <ToastContainer />
      </div>
    </div>
    </div>
  );
};

export default App;

*/